using System;
using System.Collections.Generic;
using System.Text;

namespace ClientTimeServer
{
    class BinaryMessages
    {
        /*
         * Server Messages
         */ 
        //Binary for "clienttime: "
        public byte[] ClientTime = new byte[12] { 99, 108, 105, 101, 110, 116, 116, 105, 109, 101, 58, 20 };
        //Binary for "Password: "
        public byte[] Password = new byte[10] { 112, 97, 115, 115, 119, 111, 114, 100, 58, 20 };
        //Binary for "Invalid Password"
        public byte[] InvalidPassword = new byte[16] { 105, 110, 118, 97, 108, 105, 100, 20, 112, 97, 115, 115, 119, 111, 114, 100 };
        public byte[] Voucher = new byte[9] { 118, 111, 117, 99, 104, 101, 114, 58, 20 };
        public byte[] InvalidVoucher = new byte[15] { 105, 110, 118, 97, 108, 105, 100, 20, 118, 111, 117, 99, 104, 101, 114};
        public byte[] Menu = new byte[6] { 109,101,110,117, 58, 20 };
        public byte[] InvalidMenuOption = new byte[12] { 105, 110, 118, 97, 108, 105, 100, 20, 109, 101, 110, 117};
        /*
         * Client Messages
         */
        public byte[] iClientName = new byte[255];
        public byte[] iNetworkPassword = new byte[255];
        public byte[] iVoucher = new byte[255];
        public byte[] iMenu = new byte[1];
        /*
         * Session Messages
         */
        public byte[] sClientName = new byte[255];
        public byte[] sETA = new byte[8];   //datetime is 64bits
        public byte[] sElapsed = new byte[8];
        public byte sStatus = new byte();
        /*
         * error messages
         */
    }
}
