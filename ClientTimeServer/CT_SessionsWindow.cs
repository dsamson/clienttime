using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace ClientTimeServer
{
    public partial class CT_SessionsWindow : Form
    {
        Thread SessionsThread;

        public CT_SessionsWindow()
        {
            InitializeComponent();
        }

        private void CT_CreateSession_Click(object sender, EventArgs e)
        {

            Settings s = new Settings();
            CT_Session_Create CTSC = new CT_Session_Create();
            Session result = CTSC.open();


            if (result.voucher == "")
            {
                return;
            }

            ListView.ListViewItemCollection Selected = CT_CurrentSessions.Items;
            foreach (ListViewItem item in Selected)
            {

                if (item.Text == result.voucher)
                {
                    DialogResult dr = MessageBox.Show("Session already exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
            //Add session

            s.Session_Create(result);
            Session CurrentSession = result;
            ListViewItem Item = new ListViewItem(CurrentSession.voucher, 0);

            Item.SubItems.Add(CurrentSession.status);
            Item.SubItems.Add(CurrentSession.client);

            string ETA = "";
            string Elapsed = "";


            if (CurrentSession.ETA.Hour == 0)
            {
                if (CurrentSession.ETA.Minute == 0)
                {
                    ETA = CurrentSession.ETA.Second + " Seconds";
                }
                else
                {
                    ETA = CurrentSession.ETA.Minute + " Minutes and " + CurrentSession.ETA.Second + " Seconds";
                }
            }
            if (CurrentSession.ETA.Hour > 0)
            {
                ETA = CurrentSession.ETA.Hour + " Hours and " + CurrentSession.ETA.Minute + " Minutes";
            }



            if (CurrentSession.ElapsedTime.Hour == 0)
            {
                if (CurrentSession.ElapsedTime.Minute == 0)
                {
                    Elapsed = CurrentSession.ElapsedTime.Second + " Seconds";
                }
                else
                {
                    Elapsed = CurrentSession.ElapsedTime.Minute + " Minutes and " + CurrentSession.ElapsedTime.Second + " Seconds";
                }
            }
            if (CurrentSession.ETA.Hour > 0)
            {
                Elapsed = CurrentSession.ElapsedTime.Hour + " Hours and " + CurrentSession.ElapsedTime.Minute + " Minutes";
            }



            Item.SubItems.Add(ETA);
            Item.SubItems.Add(Elapsed);

            this.CT_CurrentSessions.Items.Add(Item);

        }
          private void CT_DeleteSession_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete this session?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                //delete session
                Settings s = new Settings();
                ListView.SelectedListViewItemCollection Selected = CT_CurrentSessions.SelectedItems;
                foreach (ListViewItem item in Selected)
                {
                    s.Session_Delete(item.Text);
                    CT_CurrentSessions.Items.Remove(item);

                }
                CT_DeleteSession.Enabled = false;
            }
            //Other wise do nothing.
        }
        private void CT_CurrentSessions_SelectedIndexChanged(object sender, EventArgs e)
        {


            CT_DeleteSession.Enabled = true;
            CT_AmendTime.Enabled = true;
            CT_LockSession.Enabled = true;

            Session s = new Session();
            Settings settings = new Settings();

            ListView.SelectedListViewItemCollection Selected = CT_CurrentSessions.SelectedItems;
            foreach (ListViewItem item in Selected)
            {
                s = settings.Get_Session(item.Text);
                if (s.status.ToUpper() == "LOCKED")
                {
                    CT_LockSession.Text = "Unlock";
                }
                else
                {
                    CT_LockSession.Text = "Lock";
                }
            }
        }
        private void CT_SessionsWindow_Load(object sender, EventArgs e)
        {

            SessionsThread = new Thread(new ThreadStart(SessionsThread_handler));
            SessionsThread.Priority = ThreadPriority.Lowest;
            SessionsThread.Start();
            refreshSessions();

        }

        private void refreshSessions()
        {
            Settings s = new Settings();
            Session[] current;


            CT_CurrentSessions.Items.Clear();

            current = s.Get_Sessions();

            foreach (Session CurrentSession in current)
            {
                //do session stuff;
                ListViewItem Item = new ListViewItem(CurrentSession.voucher, 0);

                Item.SubItems.Add(CurrentSession.status);
                Item.SubItems.Add(CurrentSession.client);

                string ETA = "";
                string Elapsed = "";
                if (CurrentSession.status.ToUpper() == "ACTIVE")
                {

                    if (CurrentSession.ETA.Hour == 0)
                    {
                        if (CurrentSession.ETA.Minute == 0)
                        {
                            ETA = CurrentSession.ETA.Second + " Seconds";
                        }
                        else
                        {
                            ETA = CurrentSession.ETA.Minute + " Minutes and " + CurrentSession.ETA.Second + " Seconds";
                        }
                    }
                    if (CurrentSession.ETA.Hour > 0)
                    {
                        ETA = CurrentSession.ETA.Hour + " Hours and " + CurrentSession.ETA.Minute + " Minutes";
                    }
                }


                if (CurrentSession.ElapsedTime.Hour == 0)
                {
                    if (CurrentSession.ElapsedTime.Minute == 0)
                    {
                        Elapsed = CurrentSession.ElapsedTime.Second + " Seconds";
                    }
                    else
                    {
                        Elapsed = CurrentSession.ElapsedTime.Minute + " Minutes and " + CurrentSession.ElapsedTime.Second + " Seconds";
                    }
                }
                if (CurrentSession.ETA.Hour > 0)
                {
                    Elapsed = CurrentSession.ElapsedTime.Hour + " Hours and " + CurrentSession.ElapsedTime.Minute + " Minutes";
                }





                Item.SubItems.Add(ETA);
                Item.SubItems.Add(Elapsed);

                this.CT_CurrentSessions.Items.Add(Item);

            }
        }
        private void SessionsThread_handler()
        {
            AutoResetEvent autoEvent = new AutoResetEvent(false);
        SessionsStart:
            autoEvent.WaitOne(20000, false);
            refreshSessions();
            goto SessionsStart;
        }

        private void CT_SessionsWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (SessionsThread.IsAlive)
            {
                SessionsThread.Abort();
            }

        }

        private void CT_AmendTime_Click(object sender, EventArgs e)
        {
            CT_AmmendTimeWindow atw = new CT_AmmendTimeWindow();
            Session s = new Session();
            Settings settings = new Settings();

            ListView.SelectedListViewItemCollection Selected = CT_CurrentSessions.SelectedItems;
            foreach (ListViewItem item in Selected)
            {
                s = settings.Get_Session(item.Text);
                Session result = atw.open(s);
                settings.Session_AmmendETA(result.voucher, result.ETA);
                settings.Session_ChangeStatus(result.voucher, result.status);
                refreshSessions();
            }
        }

        private void CT_LockSession_Click(object sender, EventArgs e)
        {
            Session s = new Session();
            Settings settings = new Settings();

            ListView.SelectedListViewItemCollection Selected = CT_CurrentSessions.SelectedItems;
            foreach (ListViewItem item in Selected)
            {
                s = settings.Get_Session(item.Text);
                if (s.status.ToUpper() == "LOCKED")
                {
                    settings.Session_ChangeStatus(item.Text, "idle");
                    CT_LockSession.Text = "Lock";
                }
                else
                {
                    settings.Session_ChangeStatus(item.Text, "locked");
                    CT_LockSession.Text = "Unlock";
                }
                refreshSessions();
            }
        }

        private void button_refresh_Click(object sender, EventArgs e)
        {
            refreshSessions();
        }

     

   

       

       
    }
}