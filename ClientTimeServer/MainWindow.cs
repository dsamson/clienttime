using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using Socks;
namespace ClientTimeServer
{
    public partial class MainWindow : Form
    {
        Settings Configuration = new Settings();
        Server server;
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

            System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = false;
            if (Configuration.Activated())
            {
                this.Hide();
                server = new Server();
                server.Start();
            }
            else
            {
                this.Hide();
                CT_NotificationIcon.Visible = false;
                MessageBox.Show("Please activate product", "Activation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClientTimeServer.Activate a = new Activate();
                DialogResult d = a.open();
                if (d == DialogResult.Yes)
                {
                    CT_NotificationIcon.Visible = true;
                    
                }
                else
                {
                    this.Close();
                }
            }

        }

        private void MainWindow_Shown(object sender, EventArgs e)
        {

        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            
            this.CT_NotificationIcon.Visible = false;
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            server.Stop();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CT_PasswordWindow p = new CT_PasswordWindow();
            DialogResult dr = p.open(Password.Admin);
            if (dr == DialogResult.Yes)
            {
                //allow
                
                
            }
            else
            {
                MessageBox.Show("Access Denied", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.Close();
        }

        private void sessionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CT_SessionsWindow Sessions = new CT_SessionsWindow();
            this.sessionsToolStripMenuItem.Enabled = false;
            DialogResult Result = Sessions.ShowDialog();
            this.sessionsToolStripMenuItem.Enabled = true ;
            Sessions.Dispose();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CT_PasswordWindow p = new CT_PasswordWindow();
            DialogResult dr = p.open(Password.Admin);
            if (dr == DialogResult.Yes)
            {
                //show
                CT_SettingsWindow cts = new CT_SettingsWindow();
                this.settingsToolStripMenuItem.Enabled = false;
                DialogResult drs = cts.ShowDialog();
                this.settingsToolStripMenuItem.Enabled = true;
            }
            else
            {
                MessageBox.Show("Access Denied", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}