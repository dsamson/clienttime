using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ClientTimeServer
{
    public partial class CT_PasswordWindow : Form
    {
        bool success = false;
        Password PasswordType;
        public CT_PasswordWindow()
        {
            InitializeComponent();
        }
        public DialogResult open(Password WhichOne)
        {
            if (WhichOne == Password.Admin)
            {
                this.PasswordType = Password.Admin;
                this.Command.Text = "Please enter admin password:";
            }
            else if (WhichOne == Password.Network)
            {
                this.PasswordType = Password.Network;
                this.Command.Text = "Please enter network password:";

            }
            DialogResult d = this.ShowDialog();
            if (success)
            {
                return DialogResult.Yes;
            }
            else
            {
                return DialogResult.No;
            }
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            success = false;
            this.Close();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            Settings s = new Settings();
            Encryption krypto = new Encryption();

            bool valid = false;

            if (PasswordType == Password.Admin)
            {
                valid = s.Valid_Admin_Password(textBox_password.Text);
            }
            else if (PasswordType == Password.Network)
            {
                valid = s.Valid_Network_Password(textBox_password.Text);
            }

            if (valid)
            {
                success = true;
                this.Close();
            }
            else
            {
                success = false;
                this.Close();
            }

        }
    }
}