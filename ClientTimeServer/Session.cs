using System;
using System.Collections.Generic;
using System.Text;

namespace ClientTimeServer
{
    public class Session
    {
        //The amount of time remaining of the session
        public DateTime ETA;
        public DateTime ElapsedTime;
        //Whether the client should send a screencast of the session.
        public  bool screencast = false;
        //Whether the session is locked out
        public bool locked = false;
        //The voucher asigned with session.
        public string voucher = "";
        //The client connected with that session
        public string client = "";
        //Session status: idle, active, pause, ended, locked
        public string status = "idle";
    }
}
