using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ClientTimeServer
{
    public partial class Activate : Form
    {
        private bool ActivateSuccessfull = false;

        public Activate()
        {
            InitializeComponent();
        }
        public DialogResult open()
        {
            DialogResult dr = this.ShowDialog();
            if (ActivateSuccessfull)
            {
                
                return DialogResult.Yes;
            }
            else
            {
                return DialogResult.No;
            } 
        }
        private void BTN_Activate_Click(object sender, EventArgs e)
        {
            Settings s = new Settings();
            if (s.Valid_Serial(textBox_email.Text, TextBox_Serial.Text))
            {
                ActivateSuccessfull = true;
                s.Activate(textBox_email.Text, TextBox_Serial.Text);
                MessageBox.Show("You activation was successfull. Thank you for choosing AceTech Computing.", "Thank You", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
            }
            else
            {
                ActivateSuccessfull = false;
                MessageBox.Show("Please Re-Enter the product key.", "Invalid product key", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button_later_Click(object sender, EventArgs e)
        {
            ActivateSuccessfull = false;
            this.Close();
        }

    }
}