namespace ClientTimeServer
{
    partial class CT_NewPasswordWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CT_NewPasswordWindow));
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_change = new System.Windows.Forms.Button();
            this.TextBox_confirm = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBox_Password = new System.Windows.Forms.TextBox();
            this.Command = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_cancel
            // 
            this.button_cancel.Location = new System.Drawing.Point(204, 92);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_cancel.TabIndex = 11;
            this.button_cancel.Text = "Cancel";
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_change
            // 
            this.button_change.Location = new System.Drawing.Point(123, 92);
            this.button_change.Name = "button_change";
            this.button_change.Size = new System.Drawing.Size(75, 23);
            this.button_change.TabIndex = 10;
            this.button_change.Text = "OK";
            this.button_change.UseVisualStyleBackColor = true;
            this.button_change.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // TextBox_confirm
            // 
            this.TextBox_confirm.Location = new System.Drawing.Point(15, 66);
            this.TextBox_confirm.Name = "TextBox_confirm";
            this.TextBox_confirm.Size = new System.Drawing.Size(264, 20);
            this.TextBox_confirm.TabIndex = 9;
            this.TextBox_confirm.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Confirm";
            // 
            // TextBox_Password
            // 
            this.TextBox_Password.Location = new System.Drawing.Point(15, 26);
            this.TextBox_Password.Name = "TextBox_Password";
            this.TextBox_Password.Size = new System.Drawing.Size(264, 20);
            this.TextBox_Password.TabIndex = 7;
            this.TextBox_Password.UseSystemPasswordChar = true;
            // 
            // Command
            // 
            this.Command.AutoSize = true;
            this.Command.Location = new System.Drawing.Point(12, 9);
            this.Command.Name = "Command";
            this.Command.Size = new System.Drawing.Size(35, 13);
            this.Command.TabIndex = 6;
            this.Command.Text = "label1";
            // 
            // CT_NewPasswordWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 126);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_change);
            this.Controls.Add(this.TextBox_confirm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBox_Password);
            this.Controls.Add(this.Command);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CT_NewPasswordWindow";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "New Password";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_change;
        private System.Windows.Forms.TextBox TextBox_confirm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBox_Password;
        private System.Windows.Forms.Label Command;
    }
}