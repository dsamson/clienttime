using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ClientTimeServer
{
    public partial class CT_Session_Create : Form
    {
        private Session newSession = new Session();
       
        public CT_Session_Create()
        {
            InitializeComponent();
        }

        private void button_create_Click(object sender, EventArgs e)
        {
            //create session
            newSession.voucher = this.voucher.Text;
            newSession.locked = false;
            newSession.screencast = false;
            newSession.status = "idle";
            newSession.ElapsedTime = new DateTime();
            newSession.ETA = new DateTime(1999,2,1, Convert.ToInt32(this.numericHours.Value), Convert.ToInt32(this.numericMinutes.Value), 0);

           
            this.Close();
        }
        public Session open()
        {
            DialogResult dr = this.ShowDialog();
            
            
            return newSession;

        }

        private void CT_Session_Create_Load(object sender, EventArgs e)
        {
          
        }

       

      
    }
}