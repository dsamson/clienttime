using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ClientTimeServer
{
    public partial class CT_SettingsWindow : Form
    {
        public CT_SettingsWindow()
        {
            InitializeComponent();
        }

        private void button_admin_Click(object sender, EventArgs e)
        {
            CT_NewPasswordWindow npw = new CT_NewPasswordWindow();
            button_portnumber.Enabled = false;
            this.Enabled = false;
            npw.TopLevel = true;
            npw.TopMost = true;
            DialogResult d = npw.open(Password.Admin);
            button_portnumber.Enabled = true;
            this.Enabled = true;
        }

        private void button_password_Click(object sender, EventArgs e)
        {
            CT_NewPasswordWindow npw = new CT_NewPasswordWindow();
            button_portnumber.Enabled = false;
            this.Enabled = false;
            npw.TopLevel = true;
            npw.TopMost = true;
            DialogResult d = npw.open(Password.Network);
            button_portnumber.Enabled = true;
            this.Enabled = true;
            MessageBox.Show("Settings Changed. Note: This will not be in effect until you re-start ClientTime.");

        }

        private void button_portnumber_Click(object sender, EventArgs e)
        {

            Settings s = new Settings();
            s.Set_Network_PortNumber(PortNumber.Value.ToString());
            MessageBox.Show("Settings Changed. Note: This will not be in effect until you re-start ClientTime.");
        }

    

     
    }
}