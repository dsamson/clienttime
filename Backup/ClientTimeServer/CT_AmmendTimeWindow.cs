using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ClientTimeServer
{
    public partial class CT_AmmendTimeWindow : Form
    {

        private Session s = new Session();
        public CT_AmmendTimeWindow()
        {
            InitializeComponent();
        }

        private void button_ammend_Click(object sender, EventArgs e)
        {
            DateTime ammended = new DateTime(1999,2,1, Convert.ToInt32(this.numericHours.Value), Convert.ToInt32(this.numericMinutes.Value), 0);
            s.ETA = ammended;
            if (s.status.ToLower() == "ended")
            {
                s.status = "idle";
            }
            this.Close();
        }

        private void CT_AmmendTimeWindow_Load(object sender, EventArgs e)
        {
            numericHours.Value = Convert.ToDecimal(s.ETA.Hour);
            numericMinutes.Value = Convert.ToDecimal(s.ETA.Minute);
        }
        public Session open(Session session)
        {
            s = session;
            DialogResult dr = this.ShowDialog();
            
            return s;
        }
    }
}