using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Threading;
using ClientTimeServer;

namespace Socks
{
    public enum ServerStatus {unused =0, starting, running, idle, crashed, stopping, restarting };
    public enum ServerCrashReason { InvalidPortNumber };
    class Server
    {
 
        /*
         * Public
         */
        public ServerStatus Status;
        public ServerCrashReason CrashReason;
        public Server()
        {
            Status = ServerStatus.idle;
            CurrentSettings = new Settings();
            Thread1 = new Thread(new ThreadStart(run));

            Thread2 = new Thread(new ThreadStart(tick));
 
        }
        ~Server()
        {
            Thread1.Abort();
            Thread2.Abort();
            Status = ServerStatus.idle;
        }
        public void Start() { Status = ServerStatus.starting; if (Thread1.IsAlive) { } else { Thread1.Start(); Thread2.Start(); } }
        public void Stop() { Status = ServerStatus.stopping; if (Thread1.IsAlive) { Thread1.Abort(); Thread2.Abort(); Listen.Stop();} else { } }
        public void Restart()   { Status = ServerStatus.restarting; }
      
        /*
         * Private
         */
        private Settings CurrentSettings;
        private TcpListener Listen;
        private Socket ServerSocket;
        private int port;
        private Thread Thread1;
        private Thread Thread2;
        private Thread Thread3;
        private Thread Thread4;
        private Thread Thread5;
        private Thread Thread6;
        private Thread Thread7;
        private Thread Thread8;
        private Encryption Kryptonite = new Encryption();
        private void run()
        {
        BeginRun:
            
            if (Status == ServerStatus.unused)
            {//Unused
                goto BeginRun;
            }
            
            else if (Status == ServerStatus.starting)
            {//Starting
                try
                {
                    port = Convert.ToInt32(CurrentSettings.Get_Network_PortNumber());
                    Listen = new TcpListener(port);
                }
                catch (Exception e)
                {
                    e.Data.Clear();
                    Status = ServerStatus.crashed;
                    CrashReason = ServerCrashReason.InvalidPortNumber;
                    return;
                }

                Listen.Start(10);
                Status = ServerStatus.running;
                goto BeginRun;
            }
            #region Server Handlers
            else if (Status == ServerStatus.running)
            {//Running
                while (Status == ServerStatus.running)
                {
                    //Accept a new connection
                    ServerSocket = Listen.AcceptSocket();

                    //If a client connects
                    if (ServerSocket.Connected)
                    {
                        BinaryMessages bm = new BinaryMessages();
                        int bytes;
                        string StringBuffer = "";
                        string FinalBuffer = "";
                        
                       //Send Message "clienttime: "
                        try
                        {
                            ServerSocket.Send(bm.ClientTime);
                        }
                        catch (Exception e)
                        {
                            e.Data.Clear();
                            continue;
                        }

                        char currentchar = new char();
                        //do
                        //{
                        try
                        {
                            //Get Request
                            bytes = ServerSocket.Receive(bm.iClientName, bm.iClientName.Length, SocketFlags.None);
                            //bytes = ServerSocket.Receive(bm.iClientName);
                            //Convert to string
                            StringBuffer = Encoding.ASCII.GetString(bm.iClientName);
                            FinalBuffer = FinalBuffer + StringBuffer;
                            currentchar = Convert.ToChar(StringBuffer.Substring(StringBuffer.Length - 1, 1));
                        }
                        catch (Exception e)
                        {
                            e.Data.Clear();
                            continue;
                        }
                        
                            //} while (bytes > 1);
                            
                        
                        //filter invalid charectors
                        string client = FilterInput(FinalBuffer);

                        //Store ClientName:

                        //Clear Buffer
                        StringBuffer = "";
                        FinalBuffer = "";
                        currentchar = '\0';


                        //Send Message "Password: "
                        ServerSocket.Send(bm.Password);

                        //do
                        //{
                            //Get Request
                        try
                        {
                            bytes = ServerSocket.Receive(bm.iNetworkPassword, bm.iNetworkPassword.Length, SocketFlags.None);
                            //Convert to string
                            StringBuffer = Encoding.ASCII.GetString(bm.iNetworkPassword);
                            FinalBuffer = FinalBuffer + StringBuffer;
                            currentchar = Convert.ToChar(StringBuffer.Substring(0, 1));
                            //} while (bytes > 1);
                        }
                        catch (Exception e)
                        {
                            e.Data.Clear();
                            continue;
                        }

                        string password = FilterInput(FinalBuffer);

                        if (!(CurrentSettings.Valid_Network_Password(password)))
                        {
                            ServerSocket.Send(bm.InvalidPassword);
                            ServerSocket.Close();
                        }
                        else
                        {

                            //Clear Buffer
                            StringBuffer = "";
                            FinalBuffer = "";
                            currentchar = '\0';


                            //Send Message "Voucher: "
                            ServerSocket.Send(bm.Voucher);

                            //do
                            //{
                            try
                            {
                                //Get Request
                                bytes = ServerSocket.Receive(bm.iVoucher, bm.iVoucher.Length, SocketFlags.None);
                                //Convert to string
                                StringBuffer = Encoding.ASCII.GetString(bm.iVoucher);
                                FinalBuffer = FinalBuffer + StringBuffer;
                                currentchar = Convert.ToChar(StringBuffer.Substring(0, 1));
                            }
                            catch (Exception e)
                            {
                                e.Data.Clear();
                                continue;
                            }
                                //} while (bytes > 1);

                            //filter invalid charectors
                            string voucher = FilterInput(FinalBuffer);
                            bool ValidVoucher = false;

                            Session[] AllSession = CurrentSettings.Get_Sessions();
                            foreach (Session session in AllSession)
                            {
                                if (voucher == session.voucher)
                                {
                                    ValidVoucher = true;
                                    break;
                                }
                            }

                            if (!ValidVoucher)
                            {
                                ServerSocket.Send(bm.InvalidVoucher);
                                ServerSocket.Close();
                            }
                            else
                            {
                                Session ValidSession = CurrentSettings.Get_Session(voucher);
                                //Clear Buffer
                                StringBuffer = "";
                                FinalBuffer = "";
                                currentchar = '\0';


                                //Send Message "Menu: "
                                try
                                {
                                    ServerSocket.Send(bm.Menu);
                                }
                                catch (Exception e)
                                {
                                    e.Data.Clear();
                                    continue;
                                }
                                //do
                                //{
                                try
                                {
                                    //Get Request
                                    bytes = ServerSocket.Receive(bm.iMenu, bm.iMenu.Length, SocketFlags.None);
                                    //bytes = ServerSocket.Receive(bm.iMenu);
                                    //Convert to string
                                    StringBuffer = Encoding.ASCII.GetString(bm.iMenu);
                                    FinalBuffer = FinalBuffer + StringBuffer;
                                    currentchar = Convert.ToChar(StringBuffer.Substring(0, 1));
                                }
                                catch (Exception e)
                                {
                                    e.Data.Clear();
                                    continue;
                                }
                                    //} while (bytes > 1);

                                //filter invalid charectors
                                string menu = FilterInput(FinalBuffer);

                                if (menu.ToLower() == "s")
                                {//start session command
                                    if (ValidSession.status.ToLower() == "locked")
                                    {//do nothing
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                    else if (ValidSession.status.ToLower() == "ended")
                                    {//do nothing
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                    else if (ValidSession.status.ToLower() == "active")
                                    {//do nothing
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ValidSession.status = "active";
                                        ValidSession.client = client;
                                        CurrentSettings.Session_Create(ValidSession);
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                   
                                }
                                else if (menu.ToLower() == "p")
                                {//pause session command
                                    if (ValidSession.status.ToLower() == "paused")
                                    {//do nothing
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                    else if (ValidSession.status.ToLower() == "ended")
                                    {//do nothing
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                    else if (ValidSession.status.ToLower() == "locked")
                                    {//do nothing
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ValidSession.status = "paused";
                                        ValidSession.client = client;
                                        CurrentSettings.Session_ChangeStatus(ValidSession.voucher,  ValidSession.status);
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                }
                                else if (menu.ToLower() == "e")
                                {// end/stop session command
                                    if (ValidSession.status.ToLower() == "ended")
                                    {//do nothing
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                    else if (ValidSession.status.ToLower() == "locked")
                                    {//do nothing
                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        ValidSession.status = "ended";
                                        ValidSession.client = client;
                                        CurrentSettings.Session_ChangeStatus(ValidSession.voucher, ValidSession.status);

                                        try
                                        {
                                            ServerSocket.Send(SessionOut(ValidSession));
                                        }
                                        catch (Exception e)
                                        {
                                            e.Data.Clear();
                                            continue;
                                        }
                                    }
                                }
                                else if (menu.ToLower() == "u")
                                {//get updates command
                                    try
                                    {
                                        ServerSocket.Send(SessionOut(ValidSession));
                                    }
                                    catch (Exception e)
                                    {
                                        e.Data.Clear();
                                        continue;
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        ServerSocket.Send(bm.InvalidMenuOption);
                                    }
                                    catch (Exception e)
                                    {
                                        e.Data.Clear();
                                        continue;
                                    }
                                }
                                try
                                {
                                    ServerSocket.Close();
                                }
                                catch (Exception e)
                                {
                                    e.Data.Clear();
                                    continue;
                                }
                            }


                            
                        }
                    }
                }
            }
            #endregion
            else if (Status == ServerStatus.stopping)
            {//Running
                Listen.Stop();
                Thread1.Abort();
                Thread2.Abort();
            }
            
            else if (Status == ServerStatus.restarting)
            {//Running
                Listen.Stop();
                Thread1.Abort();
                Thread1.Start();
                Listen.Start(10);
            }
            goto BeginRun;
        }
        private void tick()
        {
            AutoResetEvent autoEvent = new AutoResetEvent(false);
        startTick:
            
            autoEvent.WaitOne(1000, false);
            Settings s = new Settings();
            Session[] sessions = s.Get_Sessions();
            foreach (Session session in sessions)
            {
                if (session.status.ToUpper() == "ACTIVE")
                {
                    /*
                     * increment elapsedtime;
                     * decrement eta
                     */
                    
                    if (session.ETA.Hour == 0 )
                    {
                        if (session.ETA.Minute == 0)
                        {
                            if (session.ETA.Second == 0)
                            {
                                s.Session_ChangeStatus(session.voucher, "ended");
                                continue;
                            }
                        }
                    }
                    if (session.ETA.Hour >=  23)
                    {
                        s.Session_ChangeStatus(session.voucher, "ended");
                        continue;
                    }

                    TimeSpan Elapsed = new TimeSpan(0, 0, 0, 1);
                    TimeSpan ETA = new TimeSpan(0, 0, 0, 1);

                    s.Session_AmmendElapsedTime(session.voucher, session.ElapsedTime.Add(Elapsed));
                    s.Session_AmmendETA(session.voucher, session.ETA.Subtract(ETA));
                    //s.Session_ChangeStatus(session.voucher,session.status);
                }
                if (session.ETA.Hour <= 0)
                {
                    if (session.ETA.Minute <= 0)
                    {
                        if (session.ETA.Second == 0)
                        {
                            s.Session_ChangeStatus(session.voucher, "ended");
                            
                        }
                    }
                }
                if (session.ETA.Hour >= 23)
                {
                    s.Session_ChangeStatus(session.voucher, "ended");
                  
                }
               
            }

           
            goto startTick;

        }

        public string FilterInput(string FinalBuffer)
        {

            //filter invalid charectors
            char[] cFinalBuffer = FinalBuffer.ToCharArray();
            FinalBuffer = "";
            foreach (char c in cFinalBuffer)
            {
                if (
                   c.ToString() != "\n"
                   && c.ToString() != "\r"
                   && c.ToString() != "\0"
                   && c != 4
                   )
                {
                    FinalBuffer = FinalBuffer + c.ToString();
                }
            }
            return FinalBuffer;
        }
        public byte[] SessionOut(Session pSession)
        {
            char[] client = pSession.client.ToCharArray();
            char[] status = pSession.status.ToCharArray();
            char[] ETA = pSession.ETA.ToString().ToCharArray();
            char[] ElapsedTime = pSession.ElapsedTime.ToString().ToCharArray();

            byte[] returnvalue = new byte[299];
            int count = 0;
            
            foreach (char c in client)
            {
                returnvalue[count] = Convert.ToByte(c);
                count++;
            }
            returnvalue[count] = 59;
            count++;
            
            foreach (char c in status)
            {
                returnvalue[count] = Convert.ToByte(c);
                count++;
            }
            returnvalue[count] = 59;
            count++;
            foreach (char c in ETA)
            {
                returnvalue[count] = Convert.ToByte(c);
                count++;
            }
            returnvalue[count] = 59;
            count++;
            foreach (char c in ElapsedTime)
            {
                returnvalue[count] = Convert.ToByte(c);
                count++;
            }
            returnvalue[count] = 59;
            count++;

            return returnvalue;
        }
    }

}
