namespace ClientTimeServer
{
    partial class CT_SessionsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CT_SessionsWindow));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.CT_CurrentSessions = new System.Windows.Forms.ListView();
            this.Session = new System.Windows.Forms.ColumnHeader();
            this.Status = new System.Windows.Forms.ColumnHeader();
            this.Client = new System.Windows.Forms.ColumnHeader();
            this.ETA = new System.Windows.Forms.ColumnHeader();
            this.Elape = new System.Windows.Forms.ColumnHeader();
            this.CT_LockSession = new System.Windows.Forms.Button();
            this.CT_DeleteSession = new System.Windows.Forms.Button();
            this.CT_AmendTime = new System.Windows.Forms.Button();
            this.CT_CreateSession = new System.Windows.Forms.Button();
            this.button_refresh = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.CT_CurrentSessions);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.button_refresh);
            this.splitContainer1.Panel2.Controls.Add(this.CT_LockSession);
            this.splitContainer1.Panel2.Controls.Add(this.CT_DeleteSession);
            this.splitContainer1.Panel2.Controls.Add(this.CT_AmendTime);
            this.splitContainer1.Panel2.Controls.Add(this.CT_CreateSession);
            this.splitContainer1.Size = new System.Drawing.Size(630, 432);
            this.splitContainer1.SplitterDistance = 396;
            this.splitContainer1.TabIndex = 0;
            // 
            // CT_CurrentSessions
            // 
            this.CT_CurrentSessions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Session,
            this.Status,
            this.Client,
            this.ETA,
            this.Elape});
            this.CT_CurrentSessions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CT_CurrentSessions.HideSelection = false;
            this.CT_CurrentSessions.LabelWrap = false;
            this.CT_CurrentSessions.Location = new System.Drawing.Point(0, 0);
            this.CT_CurrentSessions.MultiSelect = false;
            this.CT_CurrentSessions.Name = "CT_CurrentSessions";
            this.CT_CurrentSessions.Size = new System.Drawing.Size(630, 396);
            this.CT_CurrentSessions.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.CT_CurrentSessions.TabIndex = 0;
            this.CT_CurrentSessions.UseCompatibleStateImageBehavior = false;
            this.CT_CurrentSessions.View = System.Windows.Forms.View.Details;
            this.CT_CurrentSessions.SelectedIndexChanged += new System.EventHandler(this.CT_CurrentSessions_SelectedIndexChanged);
            // 
            // Session
            // 
            this.Session.Text = "Session";
            this.Session.Width = 85;
            // 
            // Status
            // 
            this.Status.Text = "Status";
            this.Status.Width = 58;
            // 
            // Client
            // 
            this.Client.Text = "Client";
            this.Client.Width = 191;
            // 
            // ETA
            // 
            this.ETA.Text = "Estimated Time Left";
            this.ETA.Width = 148;
            // 
            // Elape
            // 
            this.Elape.Text = "Total Elapsed Time";
            this.Elape.Width = 141;
            // 
            // CT_LockSession
            // 
            this.CT_LockSession.Enabled = false;
            this.CT_LockSession.Location = new System.Drawing.Point(552, 6);
            this.CT_LockSession.Name = "CT_LockSession";
            this.CT_LockSession.Size = new System.Drawing.Size(75, 23);
            this.CT_LockSession.TabIndex = 4;
            this.CT_LockSession.Text = "Lock Session";
            this.CT_LockSession.UseVisualStyleBackColor = true;
            this.CT_LockSession.Click += new System.EventHandler(this.CT_LockSession_Click);
            // 
            // CT_DeleteSession
            // 
            this.CT_DeleteSession.Enabled = false;
            this.CT_DeleteSession.Location = new System.Drawing.Point(94, 6);
            this.CT_DeleteSession.Name = "CT_DeleteSession";
            this.CT_DeleteSession.Size = new System.Drawing.Size(75, 23);
            this.CT_DeleteSession.TabIndex = 2;
            this.CT_DeleteSession.Text = "Delete Session";
            this.CT_DeleteSession.UseVisualStyleBackColor = true;
            this.CT_DeleteSession.Click += new System.EventHandler(this.CT_DeleteSession_Click);
            // 
            // CT_AmendTime
            // 
            this.CT_AmendTime.Enabled = false;
            this.CT_AmendTime.Location = new System.Drawing.Point(471, 6);
            this.CT_AmendTime.Name = "CT_AmendTime";
            this.CT_AmendTime.Size = new System.Drawing.Size(75, 23);
            this.CT_AmendTime.TabIndex = 1;
            this.CT_AmendTime.Text = "Amend Time";
            this.CT_AmendTime.UseVisualStyleBackColor = true;
            this.CT_AmendTime.Click += new System.EventHandler(this.CT_AmendTime_Click);
            // 
            // CT_CreateSession
            // 
            this.CT_CreateSession.Location = new System.Drawing.Point(13, 6);
            this.CT_CreateSession.Name = "CT_CreateSession";
            this.CT_CreateSession.Size = new System.Drawing.Size(75, 23);
            this.CT_CreateSession.TabIndex = 0;
            this.CT_CreateSession.Text = "Create Session";
            this.CT_CreateSession.UseVisualStyleBackColor = true;
            this.CT_CreateSession.Click += new System.EventHandler(this.CT_CreateSession_Click);
            // 
            // button_refresh
            // 
            this.button_refresh.Location = new System.Drawing.Point(379, 5);
            this.button_refresh.Name = "button_refresh";
            this.button_refresh.Size = new System.Drawing.Size(82, 23);
            this.button_refresh.TabIndex = 5;
            this.button_refresh.Text = "Refresh";
            this.button_refresh.UseVisualStyleBackColor = true;
            this.button_refresh.Click += new System.EventHandler(this.button_refresh_Click);
            // 
            // CT_SessionsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 432);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CT_SessionsWindow";
            this.Text = "Sessions - AceTech Client Time 2009";
            this.Load += new System.EventHandler(this.CT_SessionsWindow_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CT_SessionsWindow_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView CT_CurrentSessions;
        private System.Windows.Forms.ColumnHeader Session;
        private System.Windows.Forms.ColumnHeader Status;
        private System.Windows.Forms.ColumnHeader Client;
        private System.Windows.Forms.ColumnHeader ETA;
        private System.Windows.Forms.Button CT_AmendTime;
        private System.Windows.Forms.Button CT_CreateSession;
        private System.Windows.Forms.Button CT_DeleteSession;
        private System.Windows.Forms.Button CT_LockSession;
        private System.Windows.Forms.ColumnHeader Elape;
        private System.Windows.Forms.Button button_refresh;

    }
}