using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
namespace ClientTimeServer
{
    public enum Password { Network = 0, Admin = 1 };
    class Settings
    {
        private string SettingsKey = "SOFTWARE\\AceTech\\ClientTime\\Server";
        private string RunKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
        private string generate_serial(string email)
            {
                Encryption Kryptonite = new Encryption();
                char[,] group = new char[5, 5];
                char[] hash = Kryptonite.generate_MD5_hash(email).ToCharArray();
                string Serial = "";
                int r = 0;
                for (int g = 0; g < 5; g++)
                {
                    for (int i = 0; i < 5; i++)
                    {

                        switch (g)
                        {
                            case 0:
                                r = 0 + i;
                                break;
                            case 1:
                                r = 5 + i;
                                break;
                            case 2:
                                r = 10 + i;
                                break;
                            case 3:
                                r = 15 + i;
                                break;
                            case 4:
                                r = 20 + i;
                                break;
                        }
                        group[g, i] = hash[r];
                    }   
                }
                for (int g = 0; g < 5; g++)
                {
                    for (int i = 0; i < 5; i++)
                    {

                        Serial = Serial + group[g, i];
                    }
                    if (g < 4)
                    {
                        Serial = Serial + "-";
                    }
                }
                return Serial;
            }     

        public bool Activated()
        {
            string email;
            string serial;
         RegistryKey Settings = Registry.LocalMachine.OpenSubKey(SettingsKey);
         try
         {
              email = Settings.GetValue("Email").ToString();
              serial = Settings.GetValue("Serial").ToString();
         }
            catch(NullReferenceException e)
                {
                    email    = "";
                    serial   = "";
                    e.Data.Clear();
                }
         if (email == "")
         {
             return false;
         }
         else if (serial == "")
         {
             return false;
         }
         else if (generate_serial(email) == serial)
         {
             return true;
         }
         return false;
        }
        public void Activate(string email, string serial)
        {

            if (Valid_Serial(email, serial))
            {
                RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

                Settings.SetValue("Email", email);
                Settings.SetValue("Serial", serial);
            }
            else
            {
                return;
            }
        }
        public bool Valid_Serial(string email, string serial)
        {
            if (email == "")
            {
                return false;
            }
            else if (serial == "")
            {
                return false;
            }
            else if (generate_serial(email) == serial)
            {
                return true;
            }
            return false;
        }
        public bool Valid_Admin_Password(string password)
        {
            Encryption Kryptonite = new Encryption();
            RegistryKey Settings = Registry.LocalMachine.OpenSubKey(SettingsKey);
            string AdminPassword;
            try
            {
                AdminPassword = Settings.GetValue("Admin").ToString();
    
            }
            catch (NullReferenceException e)
            {
                AdminPassword = "";
     
                e.Data.Clear();
            }
            if (AdminPassword == "")
            {
                return true;
            }
            else if (Kryptonite.generate_sha1_HASH(AdminPassword) == Kryptonite.generate_sha1_HASH(password))
            {
                return true;
            }
            return false;
        }
        public bool Valid_Network_Password(string password)
        {
            Encryption Kryptonite = new Encryption();
            RegistryKey Settings = Registry.LocalMachine.OpenSubKey(SettingsKey);
            string AdminPassword;
            try
            {
                AdminPassword = Settings.GetValue("Network").ToString();

            }
            catch (NullReferenceException e)
            {
                AdminPassword = "";

                e.Data.Clear();
            }
            if (AdminPassword == "")
            {
                return true;
            }
            else if (Kryptonite.generate_sha1_HASH(AdminPassword) == Kryptonite.generate_sha1_HASH(password))
            {
                return true;
            }
            return false;
        }
        public void Set_Admin_Password(string password)
        {

                RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

                Settings.SetValue("Admin", password);
        }
        public void Set_Network_Password(string password)
        {

            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

            Settings.SetValue("Network", password);
        }
        public string Get_Network_Password()
        {
            string Network = "";
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);
            try
            {
                Network =  Settings.GetValue("NetworkPassword").ToString();

            }
            catch (NullReferenceException e)
            {

                e.Data.Clear();
            }
          

            return Network;
        }
        public void Set_Network_PortNumber(string PortNumber){

            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

            Settings.SetValue("PortNumber", PortNumber);
        }
        public string Get_Network_PortNumber()
        {
            string PortNumber;
             RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);
            try
            {
                PortNumber = Settings.GetValue("PortNumber").ToString();

            }
            catch (NullReferenceException e)
            {
                PortNumber = "55555";

                e.Data.Clear();
            }

            return PortNumber;
        }
        public string Get_Server()
        {
            string Network = "";
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);
            try
            {
                Network = Settings.GetValue("Server").ToString();

            }
            catch (NullReferenceException e)
            {

                e.Data.Clear();
            }


            return Network;
        }
        public void Set_Server(string Server)
        {

            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

            Settings.SetValue("Server", Server);
        
        }
        
        //Sessions
        
        public void Session_Create(string voucher, DateTime ETA)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + voucher);
            Session S = new Session();
            S.ETA = ETA;
            S.ElapsedTime = new DateTime();
            S.locked = false;
            S.screencast = false;
            S.status = "IDLE";
            
            Settings.SetValue("ETA", S.ETA);
            Settings.SetValue("ElapsedTime", S.ElapsedTime);
            Settings.SetValue("Client", S.client);
            Settings.SetValue("Screencast", S.screencast);
            Settings.SetValue("Status", S.status);
            
        }
        public void Session_Create(Session NewSession)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + NewSession.voucher);
            Session S = new Session();
            S = NewSession;
            Settings.SetValue("ETA", S.ETA);
            Settings.SetValue("ElapsedTime", S.ElapsedTime);
            Settings.SetValue("Client", S.client);
            Settings.SetValue("Screencast", S.screencast);
            Settings.SetValue("Status", S.status);

        }
        public void Session_ChangeStatus(string voucher,string Status)
        {

            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + voucher);
            Session S = new Session();
            S.status = Status;
            Settings.SetValue("Status", S.status);
        }
        public void Session_Delete(string voucher)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\");
            Settings.DeleteSubKey(voucher);
        }
        public void Session_AmmendETA(string voucher, DateTime ETA)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + voucher);
            Session S = new Session();
            S.ETA = ETA;
            Settings.SetValue("ETA", S.ETA);

        }
        public void Session_AmmendElapsedTime(string voucher, DateTime ElapsedTime)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + voucher);

            Settings.SetValue("ElapsedTime", ElapsedTime);
        
        }
        public void Session_Lock(string voucher)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + voucher);
            Session S = new Session();
            S.status = "Locked";

            Settings.SetValue("Status", S.status);

        }
        public void Session_UnLock(string voucher)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + voucher);
            Session S = new Session();
            S.status = "IDLE";

            Settings.SetValue("Status", S.status);
        }
        public void Session_Watch(string voucher, bool screencast)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + voucher);
            Session S = new Session();
            S.screencast= true;

            Settings.SetValue("Screencast", S.screencast);

        }
        public void Session_Clear()
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);
            Settings.DeleteSubKey("Sessions");
        }
        public Session Get_Session(string voucher)
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\" + voucher);
            Session S = new Session();
            S.ETA = Convert.ToDateTime(Settings.GetValue("ETA"));
            S.ElapsedTime = Convert.ToDateTime(Settings.GetValue("ElapsedTime"));
            S.screencast = Convert.ToBoolean(Settings.GetValue("Screencast"));
            S.status = Settings.GetValue("Status").ToString();
            S.voucher = voucher;
            S.client = Settings.GetValue("Client").ToString();
            return S;
        }
        public Session[] Get_Sessions()
        {
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey + "\\Sessions\\");
            String[] Sessions = Settings.GetSubKeyNames();
            Session[] S = new Session[Sessions.Length];
            for (int i = 0; i < Sessions.Length; i++)
            {
                S[i] = Get_Session(Sessions[i]);
            }
            return S;
        }
    }
}
