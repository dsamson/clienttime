using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ClientTimeClient
{
    public partial class CT_SettingsWindow : Form
    {
        public CT_SettingsWindow()
        {
            InitializeComponent();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            button_change_port.Enabled = true;
        }

        private void button_config_network_Click(object sender, EventArgs e)
        {
            CT_NewPasswordWindow npw = new CT_NewPasswordWindow();
            button_config_network.Enabled = false;
            this.Enabled = false;
            npw.TopLevel = true;
            npw.TopMost = true;
            DialogResult d = npw.open(Password.Network);
            button_config_network.Enabled = true;
            this.Enabled = true;
        }

        private void button_config_admin_Click(object sender, EventArgs e)
        {
            CT_NewPasswordWindow npw = new CT_NewPasswordWindow();
            button_config_network.Enabled = false;
            this.Enabled = false;
            npw.TopLevel = true;
            npw.TopMost = true;
            DialogResult d = npw.open(Password.Admin);
            button_config_network.Enabled = true;
            this.Enabled = true;
        }

        private void button_change_port_Click(object sender, EventArgs e)
        {
            Settings s = new Settings();
            s.Set_Server(textBox_Server.Text);
            s.Set_Network_PortNumber(PortNumber.Value.ToString());
            MessageBox.Show("Settings Changed. Note: This will not be in effect until you re-start ClientTime.");
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            button_change_port.Enabled = true;
        }

        private void CT_SettingsWindow_Load(object sender, EventArgs e)
        {
            Settings s = new Settings();
            textBox_Server.Text = s.Get_Server();
            PortNumber.Value = Convert.ToDecimal(s.Get_Network_PortNumber());
            button_change_port.Enabled = false;
        }


        

        

     
    }
}