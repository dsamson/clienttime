using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ClientTimeClient
{
    public partial class CT_ScreenCover : Form
    {
        public CT_ScreenCover()
        {
            InitializeComponent();
        }

        private void CT_ScreenCover_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt)
            {
                e.SuppressKeyPress = true;
            }
        }
    }
}