using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
namespace ClientTimeClient
{
    public enum Password { Network = 0, Admin = 1 };
    class Settings
    {
        private string SettingsKey = "SOFTWARE\\AceTech\\ClientTime\\Client";
        private string RunKey = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
        public bool Activated()
        {
            string email;
            string serial;
         RegistryKey Settings = Registry.LocalMachine.OpenSubKey(SettingsKey);
         try
         {
              email = Settings.GetValue("Email").ToString();
              serial = Settings.GetValue("Serial").ToString();
         }
            catch(NullReferenceException e)
                {
                    email    = "";
                    serial   = "";
                    e.Data.Clear();
                }
         if (email == "")
         {
             return false;
         }
         else if (serial == "")
         {
             return false;
         }
         else if (generate_serial(email) == serial)
         {
             return true;
         }
         return false;
        }
        public void Activate(string email, string serial)
        {

            if (Valid_Serial(email, serial))
            {
                RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

                Settings.SetValue("Email", email);
                Settings.SetValue("Serial", serial);
            }
            else
            {
                return;
            }
        }
        public bool Valid_Serial(string email, string serial)
        {
            if (email == "")
            {
                return false;
            }
            else if (serial == "")
            {
                return false;
            }
            else if (generate_serial(email) == serial)
            {
                return true;
            }
            return false;
        }
        public bool Valid_Admin_Password(string password)
        {
            Encryption Kryptonite = new Encryption();
            RegistryKey Settings = Registry.LocalMachine.OpenSubKey(SettingsKey);
            string AdminPassword;
            try
            {
                AdminPassword = Settings.GetValue("Admin").ToString();
    
            }
            catch (NullReferenceException e)
            {
                AdminPassword = "";
     
                e.Data.Clear();
            }
            if (AdminPassword == "")
            {
                return true;
            }
            else if (Kryptonite.generate_sha1_HASH(AdminPassword) == Kryptonite.generate_sha1_HASH(password))
            {
                return true;
            }
            return false;
        }
        public bool Valid_Network_Password(string password)
        {
            Encryption Kryptonite = new Encryption();
            RegistryKey Settings = Registry.LocalMachine.OpenSubKey(SettingsKey);
            string AdminPassword;
            try
            {
                AdminPassword = Settings.GetValue("Network").ToString();

            }
            catch (NullReferenceException e)
            {
                AdminPassword = "";

                e.Data.Clear();
            }
            if (AdminPassword == "")
            {
                return true;
            }
            else if (Kryptonite.generate_sha1_HASH(AdminPassword) == Kryptonite.generate_sha1_HASH(password))
            {
                return true;
            }
            return false;
        }
        public void Set_Admin_Password(string password)
        {

                RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

                Settings.SetValue("Admin", password);
        }
        public void Set_Network_Password(string password)
        {

            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

            Settings.SetValue("Network", password);
        }
        public string Get_Network_Password()
        {
            string Network = "";
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);
            try
            {
                Network =  Settings.GetValue("NetworkPassword").ToString();

            }
            catch (NullReferenceException e)
            {

                e.Data.Clear();
            }
          

            return Network;
        }
        public void Set_Network_PortNumber(string PortNumber){

            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

            Settings.SetValue("PortNumber", PortNumber);
        }
        public string Get_Network_PortNumber()
        {
            string PortNumber;
             RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);
            try
            {
                PortNumber = Settings.GetValue("PortNumber").ToString();

            }
            catch (NullReferenceException e)
            {
                PortNumber = "55555";

                e.Data.Clear();
            }

            return PortNumber;
        }
        public string Get_Server()
        {
            string Network = "";
            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);
            try
            {
                Network = Settings.GetValue("Server").ToString();

            }
            catch (NullReferenceException e)
            {

                e.Data.Clear();
            }


            return Network;
        }
        public void Set_Server(string Server)
        {

            RegistryKey Settings = Registry.LocalMachine.CreateSubKey(SettingsKey);

            Settings.SetValue("Server", Server);
        
        }
        private string generate_serial(string email)
        {
            Encryption Kryptonite = new Encryption();
            char[,] group = new char[5, 5];
            char[] hash = Kryptonite.generate_MD5_hash(email).ToCharArray();
            string Serial = "";
            int r = 0;
            for (int g = 0; g < 5; g++)
            {
                for (int i = 0; i < 5; i++)
                {

                    switch (g)
                    {
                        case 0:
                            r = 0 + i;
                            break;
                        case 1:
                            r = 5 + i;
                            break;
                        case 2:
                            r = 10 + i;
                            break;
                        case 3:
                            r = 15 + i;
                            break;
                        case 4:
                            r = 20 + i;
                            break;
                    }
                    group[g, i] = hash[r];
                }   
            }
            for (int g = 0; g < 5; g++)
            {
                for (int i = 0; i < 5; i++)
                {

                    Serial = Serial + group[g, i];
                }
                if (g < 4)
                {
                    Serial = Serial + "-";
                }
            }
            return Serial;
        }     
    }
}
