namespace ClientTimeClient
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.CT_NotificationIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.CT_NotificationMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.PauseSessionToolTip = new System.Windows.Forms.ToolStripMenuItem();
            this.StopStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CT_NotificationMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // CT_NotificationIcon
            // 
            this.CT_NotificationIcon.ContextMenuStrip = this.CT_NotificationMenu;
            this.CT_NotificationIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("CT_NotificationIcon.Icon")));
            this.CT_NotificationIcon.Text = "AceTech ClientTime 2009";
            this.CT_NotificationIcon.Visible = true;
            // 
            // CT_NotificationMenu
            // 
            this.CT_NotificationMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PauseSessionToolTip,
            this.StopStripMenuItem1,
            this.toolStripSeparator1,
            this.settingsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.CT_NotificationMenu.Name = "CT_NotificationMenu";
            this.CT_NotificationMenu.Size = new System.Drawing.Size(154, 126);
            // 
            // PauseSessionToolTip
            // 
            this.PauseSessionToolTip.Name = "PauseSessionToolTip";
            this.PauseSessionToolTip.Size = new System.Drawing.Size(153, 22);
            this.PauseSessionToolTip.Text = "Pause Session";
            this.PauseSessionToolTip.Click += new System.EventHandler(this.PauseSessionToolStripMenuItem_Click);
            // 
            // StopStripMenuItem1
            // 
            this.StopStripMenuItem1.Name = "StopStripMenuItem1";
            this.StopStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.StopStripMenuItem1.Text = "Stop Session";
            this.StopStripMenuItem1.Click += new System.EventHandler(this.StopSessionToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(150, 6);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(150, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 268);
            this.ControlBox = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "MainWindow";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.Shown += new System.EventHandler(this.MainWindow_Shown);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.CT_NotificationMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon CT_NotificationIcon;
        private System.Windows.Forms.ContextMenuStrip CT_NotificationMenu;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem PauseSessionToolTip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem StopStripMenuItem1;
    }
}