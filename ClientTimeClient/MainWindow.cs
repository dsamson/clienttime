using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace ClientTimeClient
{
    public partial class MainWindow : Form
    {
        Settings Configuration = new Settings();
        CT_ScreenCover sc = new CT_ScreenCover();
        CT_VoucherWindow vw = new CT_VoucherWindow();
       
        string[] commonApplications =  new string[]{    "sidebar",
                                                        "iexplore",
                                                        "wordpad",
                                                        "notepad",
                                                        "calc",
                                                        "paint",
                                                        "freecell",
                                                        "spider",
                                                        "pinball",
                                                        "sol",
                                                        "iexplore",
                                                        "msaccess",
                                                        "msword",
                                                       "msexcel",
                                                        "mspowerpoint",
                                                        "quickstart",
                                                        "sbase",
                                                        "scalc",
                                                        "sdraw",
                                                        "simpress",
                                                        "smath",
                                                        "sweb",
                                                       "swriter",
                                                        "foxit",
                                                        "7zip",
                                                        "winrar",
                                                        "eazip",
                                                        "AcroRd32",
                                                        "gimp",
                                                        "infrarecorder",
                                                        "wmplayer",
                                                        "cdplayer",
                                                        "firefox",
                                                        "thunderbird",
                                                       "sunbird",
                                                        "vlc",
                                                        "quicktime",
                                                        "realplayer",
                                                        "opera",
                                                        "safari",
                                                        "googlechrome",
                                                        "msmsgs"
                                                      };

        //Suppresses task manager etc..
        Thread Thread1;
        //closes common applications when session isn't active
        Thread Thread2;
        public void Thread1_handler()
        {
        StartThread1:
            AutoResetEvent autoEvent = new AutoResetEvent(false);
            autoEvent.WaitOne(100, false);
                /* Suppress Applications*/
            int hWnd;
                Process[] processRunning = Process.GetProcesses();
                foreach (Process pr in processRunning)
                {
                    if (pr.MainWindowTitle == "Windows Task Manager")
                    {
                        hWnd = pr.MainWindowHandle.ToInt32();

                        pr.Kill();
                    }
                    if (pr.ProcessName == "cmd")
                    {
                        hWnd = pr.MainWindowHandle.ToInt32();

                        pr.Kill();
                    }
                    if (pr.ProcessName == "ntvdm")
                    {
                        hWnd = pr.MainWindowHandle.ToInt32();

                        pr.Kill();
                    }
                    if (pr.ProcessName == "regedit")
                    {
                        hWnd = pr.MainWindowHandle.ToInt32();

                        pr.Kill();
                    }
                    if (pr.ProcessName == "regedt32")
                    {
                        hWnd = pr.MainWindowHandle.ToInt32();

                        pr.Kill();
                    } 
                    if (pr.ProcessName == "msconfig")
                    {
                        hWnd = pr.MainWindowHandle.ToInt32();

                        pr.Kill();
                    }
                }
            
            goto StartThread1;
        }
        public void Thread2_handler()
        {
            int hWnd;
            AutoResetEvent autoEvent = new AutoResetEvent(false);
        StartThread2:
            
            autoEvent.WaitOne(100, false);
            /* Suppress Applications*/
           

                Process[] processRunning = Process.GetProcesses();
                foreach (Process pr in processRunning)
                {
                        foreach (string s in commonApplications)
                        {
                            if (pr.ProcessName.ToLower() == s)
                            {
                                hWnd = pr.MainWindowHandle.ToInt32();
                                pr.Kill();
                            }
                        }
                    }
                
            goto StartThread2;
        }

        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            
                this.Hide();
                CT_NotificationIcon.Visible = false;
                CT_NotificationMenu.Enabled = true; ;
               
                Thread1 = new Thread(new ThreadStart(Thread1_handler));
                Thread1.Start();
                Thread2 = new Thread(new ThreadStart(Thread2_handler));
                Thread2.Start();

                actionHandler();
                
        }

        private void MainWindow_Shown(object sender, EventArgs e)
        {

        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.CT_NotificationIcon.Visible = false;
            
           
            
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CT_PasswordWindow p = new CT_PasswordWindow();
            CT_NotificationMenu.Enabled = false;
            DialogResult dr = p.open(Password.Admin);
            CT_NotificationMenu.Enabled = true;
            if (dr == DialogResult.Yes)
            {

                Thread1.Abort();
                if (Thread2.ThreadState == System.Threading.ThreadState.Suspended)
                {
                    Thread2.Resume();
                    
                }
                if (Thread2.ThreadState == System.Threading.ThreadState.Running)
                {
                    Thread2.Abort();
                }
            }
            else
            {
                MessageBox.Show("Access Denied", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.Close();
        }

        private void PauseSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //pause session then
            if (Thread2.ThreadState == System.Threading.ThreadState.Suspended)
            {
                Thread2.Resume();
            }
            else if (Thread2.ThreadState == System.Threading.ThreadState.Stopped)
            {
                Thread2.Start();
            }
            actionHandler();
        }
        private void StopSessionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //stop session then
            if (Thread2.IsAlive == false)
            {
                Thread2.Start();
            }
            actionHandler();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CT_SettingsWindow sw = new CT_SettingsWindow();

            CT_PasswordWindow p = new CT_PasswordWindow();
            DialogResult dr = p.open(Password.Admin);
            
            if (dr == DialogResult.Yes)
            {
                //allow

            CT_NotificationMenu.Enabled = false;
            DialogResult dra = sw.ShowDialog();
            CT_NotificationMenu.Enabled = true;
            }
            else
            {
                MessageBox.Show("Access Denied", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void actionHandler()
        {
            
            CT_NotificationIcon.Visible = false;
            CT_NotificationMenu.Enabled = true; ;

            sc.Show();
            action dr = vw.open();

            if (dr == action.Exit)
            {
                CT_NotificationIcon.Visible = false;
                this.Close();
            }
            else if (dr == action.Start)
            {
                CT_NotificationIcon.Visible = true;


                Thread2.Suspend();
             


                sc.Hide();

                PauseSessionToolTip.Enabled = true;
                StopStripMenuItem1.Enabled = true;

                settingsToolStripMenuItem.Enabled = true;
                exitToolStripMenuItem.Enabled = true;

            }
            else if (dr == action.Pause)
            {
               
                CT_NotificationIcon.Visible = false;
            }
            else if (dr == action.Stop)
            {
               
                CT_NotificationMenu.Enabled = true;
            }
            else if (dr == action.Settings)
            {
                CT_NotificationMenu.Enabled = true;
            }
                
        }
        
    }
}