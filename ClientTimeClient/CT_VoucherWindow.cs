using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Socks;

namespace ClientTimeClient
{
    public enum action{Start=0,Stop, Pause,Settings,Exit};
    public partial class CT_VoucherWindow : Form
    {
        private action actionTaken = action.Stop;
        public CT_VoucherWindow()
        {
            InitializeComponent();
        }
        public action open()
        {

            DialogResult dr = this.ShowDialog();
            this.TopLevel = true;
            this.TopMost = true;
            return this.actionTaken;

        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            

            CT_PasswordWindow p = new CT_PasswordWindow();
            p.TopMost = true;
            p.TopLevel = true;
            this.Enabled = false;
            DialogResult dr = p.open(Password.Admin);

            if (dr == DialogResult.Yes)
            {
                //allow

                actionTaken = action.Exit;
                
                this.Close();
            }
            else
            {
                MessageBox.Show("Access Denied", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Enabled = true ;
                return;
            }
        }

        private void button_settings_Click(object sender, EventArgs e)
        {
            CT_SettingsWindow sw = new CT_SettingsWindow();
            this.Enabled = false;
            CT_PasswordWindow p = new CT_PasswordWindow();
            p.TopMost = true;
            p.TopLevel = true;
            DialogResult dr = p.open(Password.Admin);

            if (dr == DialogResult.Yes)
            {
                //allow

                sw.TopMost = true;
                sw.TopLevel = true;
                DialogResult d = sw.ShowDialog();
                this.Enabled = true;
              
            }
            else
            {
                MessageBox.Show("Access Denied", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Enabled = true;
                return;
            }
        }

        private void CT_VoucherWindow_Load(object sender, EventArgs e)
        {
            this.TopLevel = true;
            this.TopMost = true;
            textBox_voucher.Clear();
            textBox_voucher.Focus();
        }

        private void CT_VoucherWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            this.actionTaken = action.Start;
            this.Close();
        }
    }
}