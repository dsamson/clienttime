using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ClientTimeClient
{
    public partial class CT_NewPasswordWindow : Form
    {
        public CT_NewPasswordWindow()
        {
            InitializeComponent();
        }
        bool success = false;
        Password PasswordType;
       
        public DialogResult open(Password WhichOne)
        {
            if (WhichOne == Password.Admin)
            {
                this.PasswordType = Password.Admin;
                this.Command.Text = "Please enter a new admin password:";
            }
            else if (WhichOne == Password.Network)
            {
                this.PasswordType = Password.Network;
                this.Command.Text = "Please enter a new network password:";

            }
            DialogResult d = this.ShowDialog();
            if (success)
            {
                return DialogResult.Yes;
            }
            else
            {
                return DialogResult.No;
            }
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            success = false;
            this.Close();
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            Settings s = new Settings();

            Encryption krypto = new Encryption();

            if (TextBox_Password.Text == TextBox_confirm.Text)
            {
                if (PasswordType == Password.Admin)
                {
                    s.Set_Admin_Password(krypto.generate_sha1_HASH(TextBox_Password.Text));
                }
                else if (PasswordType == Password.Network)
                {
                    s.Set_Network_Password(krypto.generate_sha1_HASH(TextBox_Password.Text));
                    MessageBox.Show("Settings Changed. Note: This will not be in effect until you re-start ClientTime.");

                }

                this.success = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Your passwords do not match. Please re-enter.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TextBox_confirm.Clear();
                TextBox_Password.Clear();
                TextBox_Password.Focus();
            }
            

        }

       
    }

}