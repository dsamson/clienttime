using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net.Security;
using System.Threading;

using ClientTimeClient;


namespace Socks
{
    class Client
    {
        private Settings CurrentSettings = new Settings();
        
        private Socket ClientSocket;
      
        public void connect()
        {
            ClientSocket = new Socket(AddressFamily.NetBios, SocketType.Stream, ProtocolType.Tcp);
            ClientSocket.Connect(CurrentSettings.Get_Server(), Convert.ToInt32(CurrentSettings.Get_Network_PortNumber()));
            if (ClientSocket.Connected)
            {
                MessageBox.Show("Connected");
            }
            else
            {
                MessageBox.Show("Not Connected");
            }
            }
        }
    
}

