using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace ClientTimeClient
{
    class Encryption
    {
        public string generate_sha1_HASH(string plaintext)
        {
            char[] text = plaintext.ToCharArray();
            byte[] data = new byte[text.Length];
            byte[] result;
            SHA1 Hash = new SHA1Managed();
            string returnvalue = "";

            for (int index = 0; index < text.Length; index++)
            {
                data[index] = Convert.ToByte(text[index]);
            }


            result = Hash.ComputeHash(data);

            for (int index = 0; index < result.Length; index++)
            {
                returnvalue = returnvalue + Convert.ToString(result[index]);
            }

            return returnvalue;

        }
        public string generate_MD5_hash(string plaintext)
        {
            char[] text = plaintext.ToCharArray();
            byte[] data = new byte[text.Length];
            byte[] result;
            MD5 Hash = new MD5CryptoServiceProvider();
            string returnvalue = "";

            for (int index = 0; index < text.Length; index++)
            {
                data[index] = Convert.ToByte(text[index]);
            }


            result = Hash.ComputeHash(data);

            for (int index = 0; index < result.Length; index++)
            {
                returnvalue = returnvalue + Convert.ToString(result[index]);
            }

            return returnvalue;

        }


    }
}
