namespace ClientTimeClient
{
    partial class CT_SettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CT_SettingsWindow));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_config_admin = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_config_network = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Server = new System.Windows.Forms.TextBox();
            this.PortNumber = new System.Windows.Forms.NumericUpDown();
            this.button_change_port = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PortNumber)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_config_admin);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(15, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 89);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Admin Password";
            // 
            // button_config_admin
            // 
            this.button_config_admin.Location = new System.Drawing.Point(187, 61);
            this.button_config_admin.Name = "button_config_admin";
            this.button_config_admin.Size = new System.Drawing.Size(75, 23);
            this.button_config_admin.TabIndex = 0;
            this.button_config_admin.Text = "Configure";
            this.button_config_admin.UseVisualStyleBackColor = true;
            this.button_config_admin.Click += new System.EventHandler(this.button_config_admin_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(7, 20);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(255, 55);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "If configured the admin password will prevent any un-authorised access from chang" +
                "ing settings or closing the server.";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_config_network);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Location = new System.Drawing.Point(15, 114);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(268, 80);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Network Password";
            // 
            // button_config_network
            // 
            this.button_config_network.Location = new System.Drawing.Point(187, 52);
            this.button_config_network.Name = "button_config_network";
            this.button_config_network.Size = new System.Drawing.Size(75, 23);
            this.button_config_network.TabIndex = 1;
            this.button_config_network.Text = "Configure";
            this.button_config_network.UseVisualStyleBackColor = true;
            this.button_config_network.Click += new System.EventHandler(this.button_config_network_Click);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Control;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(7, 20);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(255, 55);
            this.textBox2.TabIndex = 0;
            this.textBox2.Text = "This prevents un-authorised computers from using this server.";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.textBox_Server);
            this.groupBox3.Controls.Add(this.PortNumber);
            this.groupBox3.Controls.Add(this.button_change_port);
            this.groupBox3.Location = new System.Drawing.Point(15, 200);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(268, 125);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Network Settings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Port:";
         
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Server";
            // 
            // textBox_Server
            // 
            this.textBox_Server.Location = new System.Drawing.Point(6, 35);
            this.textBox_Server.Name = "textBox_Server";
            this.textBox_Server.Size = new System.Drawing.Size(255, 20);
            this.textBox_Server.TabIndex = 4;
            this.textBox_Server.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // PortNumber
            // 
            this.PortNumber.Location = new System.Drawing.Point(6, 80);
            this.PortNumber.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.PortNumber.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.PortNumber.Name = "PortNumber";
            this.PortNumber.Size = new System.Drawing.Size(174, 20);
            this.PortNumber.TabIndex = 3;
            this.PortNumber.Value = new decimal(new int[] {
            55555,
            0,
            0,
            0});
            this.PortNumber.ValueChanged += new System.EventHandler(this.numericUpDown2_ValueChanged);
            // 
            // button_change_port
            // 
            this.button_change_port.Enabled = false;
            this.button_change_port.Location = new System.Drawing.Point(186, 77);
            this.button_change_port.Name = "button_change_port";
            this.button_change_port.Size = new System.Drawing.Size(75, 23);
            this.button_change_port.TabIndex = 2;
            this.button_change_port.Text = "Change";
            this.button_change_port.UseVisualStyleBackColor = true;
            this.button_change_port.Click += new System.EventHandler(this.button_change_port_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox1);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Location = new System.Drawing.Point(12, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(298, 331);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            // 
            // CT_SettingsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 360);
            this.Controls.Add(this.groupBox4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CT_SettingsWindow";
            this.Text = "Settings - ClientTime 2009";
            this.Load += new System.EventHandler(this.CT_SettingsWindow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PortNumber)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_config_network;
        private System.Windows.Forms.Button button_config_admin;
        private System.Windows.Forms.Button button_change_port;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown PortNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Server;
        private System.Windows.Forms.Label label2;
    }
}